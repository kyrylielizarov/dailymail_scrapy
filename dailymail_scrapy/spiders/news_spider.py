import scrapy
import json
import datetime
import pytz
from scrapy.utils.response import open_in_browser


class NewsSpider(scrapy.Spider):
    name = "newslist"

    def __init__(self, num_news='30', *args, **kwargs):
        super(NewsSpider, self).__init__(*args, **kwargs)
        self.start_urls = []
        for num in range(0, int(num_news), 30):
            self.start_urls.append('http://www.dailymail.co.uk/api/latest_headlines/home/{0}.json'.format(num))

    def parse(self, response):
        base_url = 'http://www.dailymail.co.uk'
        res_json = json.loads(response.body)
        count = 0
        for part in res_json:
            count += 1
            page_url = base_url + part['articleURL']
            article_dt = datetime.datetime.strptime(part['firstPubDate'], "%Y-%m-%dT%H:%M:%S%z")
            article_dt_str = article_dt.astimezone(pytz.utc).strftime('%Y-%m-%d %H:%M:%S %Z')
            article = {
                'title': part['pageTitle'],
                'headline': part['headline'],
                'url': page_url,
                'previewText': part['largePreviewText'],
                'publicationDate': article_dt_str,
                'metaKeywords': part['metaKeywords'],
                'articleImage': part['articleImage']['url'],
                'categoty': part['channel']
            }
            r = scrapy.Request(url=page_url, callback=self.parse_news)
            r.meta['article'] = article
            yield r
        self.log("Number of atricles: {0}".format(count))

    def parse_news(self, response):
        self.log("Visited: {0}".format(response.url))
        article = response.meta['article']
        self.log("Title: *** {0} ***".format(article['title']))
        res_text = ''
        count = 1
        for text in response.css('p.mol-para-with-font::text').extract():
            count += 1
            res_text = res_text + '\n' + text
        article['text'] = res_text
        yield article


